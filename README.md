# Introduction #

This is a plugin for the [Apache Traffic Server](http://trafficserver.apache.org/) (ATS) that performs transparent URL
rewriting.  On its own it is not very useful.  It is a component of a larger URL remapping service built for the
Mogotest Web Consistency Testing service.

For more details, please see the [Mogotest URL remapper](https://bitbucket.org/mogotest/url_remapper) project.

The code is licensed under the Apache License, version 2.0. This project is no longer maintained but was kept up-to-date
as of ATS 4.2.0.

# Building #

ATS includes its own wrapper utility for compiling plugins.  Once you've cloned the project, you
can build and install it via:

```
#!bash

$ $ATS_HOME/bin/tsxs -o mogotest_remapper.so -c mogotest_remapper.cc
$ mv mogotest_remapper.so $ATS_HOME/libexec/trafficserver/
```

# Configuring #

You enable and configure this plugin by adding it to the ATS plugin.config file:

```
mogotest_remapper.so <url_remapper_host_ip> <url_remapper_host_port> <shared_secret_token>
```

The `url_remapper_host` is the web server running the Mogotest URL remapper application.  The `shared_secret_token`
is the simple authentication mechanism used for queries made to the Mogotest URL remapper application; both sides
must agree on the token for the request to be processed.
