#include <ts/ts.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

using std::string;

static const char* PLUGIN_NAME = "mogotest_remapper";

static const int FETCH_EVENT_SUCCESS = 10000;
static const int FETCH_EVENT_FAILURE = 10001;
static const int FETCH_EVENT_TIMEOUT = 10002;

typedef struct
{
  const char* api_host_name;
  const char* api_host_port;
  const char* secret_token;
} api_config_container;

/**
 * Cleans up resources allocated while examining request objects.
 *
 * @param The request.
 * @param A handle to the headers in the request.
 * @param A handle to the URL location.
 * @param A handle to the 'Host' header in the headers.
 */
void cleanup(TSMBuffer reqp, TSMLoc hdr_loc, TSMLoc url_loc, TSMLoc field_loc)
{
  if (field_loc)
  {
    TSHandleMLocRelease(reqp, hdr_loc, field_loc);
  }

  if (url_loc)
  {
    TSHandleMLocRelease(reqp, hdr_loc, url_loc);
  }

  if (hdr_loc)
  {
    TSHandleMLocRelease(reqp, TS_NULL_MLOC, hdr_loc);
  }
}

/**
 * Given an HTTP response, which consists of the MIME headers, followed by "\r\n\r\n" and the response body text,
 * extract out the response body text.
 *
 * @param response The full HTTP response.
 * @param response_length The length of the `response` string.
 *
 * @return A string containing only the response body text.
 */
const char* extract_body(const char* response, int response_length)
{
  for (int i = 0; i < response_length; i++)
  {
    // Look for "\r\n\r\n" and then offset the string by that plus 1, in order to point at the first character
    // of the response body.
    if (((i + 4) < response_length) && (response[i] == '\r') && (response[i + 1] == '\n') && (response[i + 2] == '\r') && (response[i + 3] == '\n'))
    {
      return response + (i + 4);
    }
  }

  // If we can't extract the response body for any reason, just return the original response so we still have a valid string.
  return response;
}

bool do_mogotest_remap(TSHttpTxn original_transaction, TSHttpTxn api_transaction)
{
  TSMBuffer reqp;
  TSMLoc hdr_loc, url_loc, field_loc = NULL;

  // Read the original request.
  if (TSHttpTxnClientReqGet(original_transaction, &reqp, &hdr_loc) != TS_SUCCESS)
  {
    TSDebug(PLUGIN_NAME, "could not get request data");
    return false;
  }

  // Get a handle to the URL value.
  TSHttpHdrUrlGet(reqp, hdr_loc, &url_loc);
  if (! url_loc)
  {
    TSDebug(PLUGIN_NAME, "couldn't retrieve request URL");
    cleanup(reqp, hdr_loc, url_loc, field_loc);

    return false;
  }

  // Get a handle to the HOST header.
  field_loc = TSMimeHdrFieldFind(reqp, hdr_loc, TS_MIME_FIELD_HOST, TS_MIME_LEN_HOST);
  if (! field_loc)
  {
    TSDebug(PLUGIN_NAME, "couldn't retrieve HOST header");
    cleanup(reqp, hdr_loc, url_loc, field_loc);

    return false;
  }

  // Get the API response body.
  int respLength;
  const char * resp = TSFetchRespGet(api_transaction, &respLength);
  const char * body = extract_body(resp, respLength);
  char* body_copy = strndup(body, 1024);

  // Parse the API response into its components.
  char* hostname = strtok(body_copy, ":");
  char* port = strtok(NULL, ":");
  char* scheme = strtok(NULL, ":");

  TSDebug(PLUGIN_NAME, "API response -- full body: %s; parsed host: %s; parsed port: %s, parsed scheme: %s",\
    body, hostname, port, scheme);

  // Remap the original request with the values from the API response.
  TSMimeHdrFieldValueStringSet(reqp, hdr_loc, field_loc, 0, hostname, -1);
  TSUrlHostSet(reqp, url_loc, hostname, -1);
  TSUrlSchemeSet(reqp, url_loc, scheme, -1);
  TSUrlPortSet(reqp, url_loc, atoi(port));

  // Release any allocated resources.
  free(body_copy);
  cleanup(reqp, hdr_loc, url_loc, field_loc);

  return true;
}

static int mogotest_api_request_callback(TSCont contp, TSEvent event, void* edata)
{
  TSDebug(PLUGIN_NAME, "API Request Callback");
  TSDebug(PLUGIN_NAME, "Event: %d", event);

  TSHttpTxn txnp = (TSHttpTxn) edata;
  TSHttpTxn originalTxnp = (TSHttpTxn) TSContDataGet(contp);

  switch((int) event)
  {
    case FETCH_EVENT_SUCCESS:
      do_mogotest_remap(originalTxnp, txnp);
      break;

    default:
      break;
  }

  TSHttpTxnReenable(originalTxnp, TS_EVENT_HTTP_CONTINUE);

  return 1;
}

bool lookup_remap(TSCont contp, TSHttpTxn txnp)
{
  TSMBuffer reqp;
  TSMLoc hdr_loc, url_loc, field_loc = NULL;
  const char * request_host;
  int request_host_length = 0;
  const char * request_scheme;
  int request_scheme_length = 0;
  int request_port = 80;
  struct addrinfo hints;
  struct addrinfo* servinfo;
  int ret;

  // Read the original request.
  if (TSHttpTxnClientReqGet(txnp, &reqp, &hdr_loc) != TS_SUCCESS)
  {
    TSDebug(PLUGIN_NAME, "could not get request data");
    return false;
  }

  // Get a handle to the URL value.
  TSHttpHdrUrlGet(reqp, hdr_loc, &url_loc);
  if (! url_loc)
  {
    TSDebug(PLUGIN_NAME, "couldn't retrieve request URL");
    cleanup(reqp, hdr_loc, url_loc, field_loc);

    return false;
  }

  // Get a handle to the HOST header.
  field_loc = TSMimeHdrFieldFind(reqp, hdr_loc, TS_MIME_FIELD_HOST, TS_MIME_LEN_HOST);
  if (! field_loc)
  {
    TSDebug(PLUGIN_NAME, "couldn't retrieve HOST header");
    cleanup(reqp, hdr_loc, url_loc, field_loc);

    return false;
  }

  // Read in the requested host value.
  request_host = TSMimeHdrFieldValueStringGet(reqp, hdr_loc, field_loc, 0, &request_host_length);
  if (! request_host_length)
  {
    TSDebug(PLUGIN_NAME, "couldn't find request HOST header");
    cleanup(reqp, hdr_loc, url_loc, field_loc);

    return false;
  }

  request_scheme = TSUrlSchemeGet(reqp, url_loc, &request_scheme_length);
  request_port   = TSUrlPortGet(reqp, url_loc);

  TSDebug(PLUGIN_NAME,"Original request -- scheme: %.*s; host: %.*s; port: %d",\
    request_scheme_length,\
    request_scheme,\
    request_host_length,\
    request_host,\
    request_port
  );


  // Look up address of API host.
  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;

  api_config_container* api_config = (api_config_container *) TSContDataGet(contp);

  TSDebug(PLUGIN_NAME, "API config -- hostname: %s; port: %s; secret_token: %s", api_config->api_host_name, api_config->api_host_port, api_config->secret_token);

  ret = getaddrinfo(api_config->api_host_name, api_config->api_host_port, &hints, &servinfo);
  if (ret != 0)
  {
    TSDebug(PLUGIN_NAME, "getaddrinfo: %s", gai_strerror(ret));
  }

  // The `request_host` may contain more than just the hostname and `request_host_length` indicates where the hostname portion ends.
  char request_host_name[request_host_length + 1];
  snprintf(request_host_name, request_host_length + 1, "%.*s", (int) sizeof(request_host_name), request_host);

  // The `request_scheme` may contain more than just the scheme and `request_scheme_length` indicates where the scheme portion ends. 
  char request_host_scheme[request_scheme_length + 1];
  snprintf(request_host_scheme, request_scheme_length + 1, "%.*s", (int) sizeof(request_host_scheme), request_scheme);

  char request_port_as_string[21]; // Enough to hold all numbers up to 64-bits.
  snprintf(request_port_as_string, sizeof(request_port_as_string), "%d", request_port);


  // Build up the remap remote API request.
  string request("");
  request.append("GET /internal/remapper/?host=");
  request.append(request_host_name);
  request.append("&port=");
  request.append(request_port_as_string);
  request.append("&scheme=");
  request.append(request_host_scheme);
  request.append("&secret_token=");
  request.append(api_config->secret_token);
  request.append(" HTTP/1.0\r\n");
  request.append("Host:");
  request.append(api_config->api_host_name);
  request.append(":");
  request.append(api_config->api_host_port);
  request.append("\r\n\r\n");

  TSFetchEvent eventIds;
  eventIds.success_event_id = FETCH_EVENT_SUCCESS;
  eventIds.failure_event_id = FETCH_EVENT_FAILURE;
  eventIds.timeout_event_id = FETCH_EVENT_TIMEOUT;

  TSDebug(PLUGIN_NAME, "Making API request: %s", request.data());

  // Make the API request, passing the original request as context data for the request callback.
  TSCont cont = TSContCreate(mogotest_api_request_callback, TSMutexCreate());
  TSContDataSet(cont, (void *) txnp);
  TSFetchUrl(request.data(), request.size(), servinfo->ai_addr, cont, AFTER_BODY, eventIds);

  // Release any allocated resources.
  freeaddrinfo(servinfo);
  cleanup(reqp, hdr_loc, url_loc, field_loc);

  return true;
}

static int mogotest_remap(TSCont contp, TSEvent event, void* edata)
{
  TSHttpTxn txnp = (TSHttpTxn) edata;

  TSDebug(PLUGIN_NAME, "Here I am");
  TSDebug(PLUGIN_NAME, "Event: %d", event);

  switch(event)
  {
    case TS_EVENT_HTTP_PRE_REMAP:
      TSDebug(PLUGIN_NAME, "Reading request");
      TSSkipRemappingSet(txnp, 1);

      if (TSHttpIsInternalRequest(txnp) == TS_ERROR)
      {
        TSDebug(PLUGIN_NAME, "External");
        lookup_remap(contp, txnp);
      }
      else
      {
        TSDebug(PLUGIN_NAME, "Internal");
        TSHttpTxnReenable(txnp, TS_EVENT_HTTP_CONTINUE);
      }
      break;

    default:
      TSDebug(PLUGIN_NAME, "Internal Event: %d", event);
      break;
  }

  TSDebug(PLUGIN_NAME, "Rolling out.");

  return 1;
}

void TSPluginInit(int argc, const char* argv[])
{
  TSPluginRegistrationInfo info;

  info.plugin_name = (char *) "MogotestRemapper";
  info.vendor_name = (char *) "Mogotest";
  info.support_email = (char *) "kevin@mogotest.com";

  if (TSPluginRegister(TS_SDK_VERSION_3_0, &info) != TS_SUCCESS)
  {
    TSError("mogotest_remapper: plugin registration failed");
  }

  if (argc != 4)
  {
    TSDebug(PLUGIN_NAME, "mogotest_remapper usage: <api_host> <api_port> <secret_token>");
    return;
  }

  api_config_container* api_config = new api_config_container;
  api_config->api_host_name = strndup(argv[1], 1024);
  api_config->api_host_port = strndup(argv[2], 1024);
  api_config->secret_token = strndup(argv[3], 1024);

  TSCont cont = TSContCreate(mogotest_remap, NULL);
  TSContDataSet(cont, api_config);
  TSHttpHookAdd(TS_HTTP_READ_REQUEST_PRE_REMAP_HOOK, cont);
}
